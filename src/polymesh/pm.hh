#pragma once

// Includes all polymesh features

#include <polymesh/Mesh.hh>

#include <polymesh/algorithms/optimization.hh>
#include <polymesh/algorithms/properties.hh>

#include <polymesh/formats.hh>
#include <polymesh/objects.hh>
