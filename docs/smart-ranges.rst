Smart Ranges
============

Many collections found in ``polymesh::`` derive from :cpp:struct:`polymesh::smart_range\<this_t, ElementT>`.

:cpp:struct:`polymesh::vertex_attribute\<T>`

:cpp:struct:`polymesh::vertex_attribute`

:cpp:struct:`polymesh::smart_range`

:cpp:struct:`smart_range`

TODO
