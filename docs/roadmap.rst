Roadmap
=======

TODO

Refactorings
------------

* simplify ``make_attribute`` variants and remove the one that is achieved via ``map``
* provide a ``span`` type
* remove ``std`` dependencies where appropriate to reduce compile times

Long Term
---------

* add more algorithms
* add more formats
* add more objects
* re-add binary polymesh format with arbitrary attributes
* sparse attributes
