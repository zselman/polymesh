Class Reference
===============

Mesh
----

.. doxygenclass:: polymesh::Mesh
    :members:

Handles and Indices
-------------------

.. doxygenstruct:: polymesh::primitive_index
    :members:

.. doxygenstruct:: polymesh::face_index
    :members:

.. doxygenstruct:: polymesh::vertex_index
    :members:

.. doxygenstruct:: polymesh::edge_index
    :members:

.. doxygenstruct:: polymesh::halfedge_index
    :members:

.. doxygenstruct:: polymesh::primitive_handle
    :members:

.. doxygenstruct:: polymesh::face_handle
    :members:

.. doxygenstruct:: polymesh::vertex_handle
    :members:

.. doxygenstruct:: polymesh::edge_handle
    :members:

.. doxygenstruct:: polymesh::halfedge_handle
    :members:

Ranges and Collections
----------------------

.. doxygenstruct:: polymesh::smart_range
    :members:

.. doxygenstruct:: polymesh::smart_collection
    :members:

.. doxygenstruct:: polymesh::face_collection
    :members:

.. doxygenstruct:: polymesh::vertex_collection
    :members:

.. doxygenstruct:: polymesh::edge_collection
    :members:

.. doxygenstruct:: polymesh::halfedge_collection
    :members:

Attributes
----------

.. doxygenstruct:: polymesh::vertex_attribute
    :members:

.. doxygenstruct:: polymesh::face_attribute
    :members:

.. doxygenstruct:: polymesh::edge_attribute
    :members:

.. doxygenstruct:: polymesh::halfedge_attribute
    :members:

.. doxygenstruct:: polymesh::primitive_attribute
    :members:

.. doxygenstruct:: polymesh::primitive_attribute_base
    :members:

Low-Level API
-------------

.. doxygenstruct:: polymesh::low_level_api_base
    :members:

.. doxygenstruct:: polymesh::low_level_api_mutable
    :members:

Helper
------

.. doxygenfunction:: polymesh::copy

.. doxygenstruct:: polymesh::unique_ptr

.. doxygenstruct:: polymesh::unique_array


Internals
---------

.. doxygenstruct:: polymesh::vertex_tag

.. doxygenstruct:: polymesh::face_tag

.. doxygenstruct:: polymesh::edge_tag

.. doxygenstruct:: polymesh::halfedge_tag
